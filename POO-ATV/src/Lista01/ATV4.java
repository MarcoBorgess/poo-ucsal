package Lista01;

import java.util.Scanner;

public class ATV4 {
    public static void main(String[] args) {
        int qnt = 2;
        String[][] info = pedirInfo(qnt);
        System.out.println(maisPesado(info));
        System.out.println(maisAlto(info));
    }
    public static String[][] pedirInfo(int qnt){
        String[][] array = new String[qnt][3];
        Scanner sc = new Scanner(System.in);
        for (int i = 0; i < array.length; i++){
            System.out.println("Nome (Pessoa "+(i+1)+"): ");
            array[i][0] = sc.nextLine();
            System.out.println("Altura (Pessoa "+(i+1)+"): ");
            array[i][1] = sc.nextLine();
            System.out.println("Peso (Pessoa "+(i+1)+"): ");
            array[i][2] = sc.nextLine();
        }
        return array;
    }
    public static String maisPesado(String[][] info){
        double maisPesado = 0;
        String maisPesadoNome = "";
        for (int i = 0; i < info.length; i++){
            if (Double.parseDouble(info[i][2]) > maisPesado){
                maisPesado = Double.parseDouble(info[i][2]);
                maisPesadoNome = info[i][0];
            }
        }
        String txt = "A pessoa mais pesada é "+maisPesadoNome+" com "+maisPesado+" kilos.";
        return txt;
    }
    public static String maisAlto(String[][] info){
        double maisAlto = 0;
        String maisAltoNome = "";
        for (int i = 0; i < info.length; i++){
            if (Double.parseDouble(info[i][1]) > maisAlto){
                maisAlto = Double.parseDouble(info[i][1]);
                maisAltoNome = info[i][0];
            }
        }
        String txt = "A pessoa mais alta é "+maisAltoNome+" com "+maisAlto+"m.";
        return txt;
    }
}

