package Lista01;

import java.util.Arrays;
import java.util.Scanner;

public class ATV5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Número de Termos:");
        int n = sc.nextInt();
        System.out.println("Primeiro termo da progressão:");
        int a1 = sc.nextInt();
        System.out.println("Razão da progressão:");
        int r = sc.nextInt();

        System.out.println(Arrays.toString(getPA(n, a1, r)));
        System.out.println("Soma dos termos: "+somaPA(getPA(n, a1, r)));
    }
    public static int[] getPA(int termos, int inicial, int razao){
        int[] pa = new int[termos];
        int valor = inicial;
        for (int i = 0; i < pa.length; i++){
            pa[i] = valor;
            valor += razao;
        }
        return pa;
    }
    public static int somaPA(int[] pa){
        int valor = 0;
        for (int i = 0; i < pa.length; i++){
            valor += pa[i];
        }
        return valor;
    }
}
