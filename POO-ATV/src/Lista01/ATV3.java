package Lista01;

import java.util.Scanner;

public class ATV3 {
    public static void main(String[] args) {
        int qnt = 10;
        int[] array = pedirInteiros(qnt);

        System.out.println("Média: "+calcMedia(array));
        System.out.println("Maior: "+getMaior(array));
        System.out.println("Menor: "+getMenor(array));
    }

    private static int[] pedirInteiros(int qnt){
        Scanner sc = new Scanner(System.in);
        int[] array = new int[qnt];
        for (int i = 0; i < qnt; i++){
            System.out.println("Digite um número:");
            array[i] = sc.nextInt();
        }
        return array;
    }

    private static int calcMedia(int[] array){
        int media = 0;
        for (int i = 0; i < array.length; i++){
            media += array[i];
        }
        media = media/array.length;
        return  media;
    }

    private static int getMaior(int[] array){
        int maior = Integer.MIN_VALUE;
        for (int i = 0; i < array.length; i++){
            if (array[i] > maior){
                maior = array[i];
            }
        }
        return maior;
    }

    private static int getMenor(int[] array){
        int menor = Integer.MAX_VALUE;
        for (int i = 0; i < array.length; i++){
            if (array[i] < menor){
                menor = array[i];
            }
        }
        return menor;
    }
}
