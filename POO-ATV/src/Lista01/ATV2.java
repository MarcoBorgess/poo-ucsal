package Lista01;

import java.util.Scanner;

public class ATV2 {
    public static void main(String[] args) {
        calculaE();
    }
    private static void calculaE(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Digite um número:");
        double num = sc.nextDouble();
        double x = 1;
        for (int i = 1; i <= num; i++){
            x += 1/calcFatorial(i);
        }
        System.out.println(x);
    }
    private static double calcFatorial(double x){
        double f = x;
        while (x > 1){
            f=f*(x-1);
            x--;
        }
        return f;
    }
}
