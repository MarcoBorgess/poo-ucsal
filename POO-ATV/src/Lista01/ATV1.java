package Lista01;

import java.util.Scanner;

public class ATV1 {
    public static void main(String[] args) {
        getConceito();
    }
    private static void getConceito() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Digite a nota:");
        int num = sc.nextInt();
        if (num <= 49) {
            System.out.println("Insuficiente");
        }else if (num <= 64) {
            System.out.println("Regular");
        }else if (num <= 84) {
            System.out.println("Bom");
        }else if (num <= 100) {
            System.out.println("Ótimo");
        }
    }
}
